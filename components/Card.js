import React from 'react';
import {View,Text,Image,TouchableOpacity,StyleSheet} from 'react-native';

const Card= ()=>{
    return(
        <View style={styles.container}>
            <View style={styles.containerHeader}>
                <Text style={styles.textHeader}>CIUDADES HERMOSAS</Text>
            </View>
            <View style={styles.containerImage}>
                <Image 
                source={require('./Camiri.jpg')} 
                style={styles.image}
                />
            </View>
            <View style={styles.containerTittle}>
                <Text style={styles.textTitulo}>Camiri</Text>
                <Text style={styles.textTituloDescripcion}>Ciudad en Bolivia</Text>
            </View>
            <View style={styles.containerSubtittle}>
                <Text style={styles.textSubtitulo}>
                    Camiri es una ciudad y municipio, 
                    conocida como la capital petrolera de Bolivia, 
                    ubicada en la provincia de Cordillera en el departamento de Santa Cruz.
                </Text>
                <Text style={styles.textSubtitulo}>
                    Sus principales actividades económicas son la ganadería y la petrolera. 
                    El municipio de Camiri es rico también en cultura. 
                    Es el eje central y el núcleo urbano más poblado de esta provincia, cuenta con 33.838habitantes.
                </Text>
            </View>
            <View style={styles.containerbutton}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={()=>{console.log('Estoy feliz de crear esto JM')}}
                >
                    <Text style={styles.buttonText}>VER LUGARES TURÍSTICOS</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create ({
    container:{
        //paddingHorizontal:10,
        //paddingVertical:10,
        flex:1,
        flexDirection:'column',
        backgroundColor:'#fff',
    },
    containerHeader:{
        flex:1,
        //backgroundColor:'#1B5A6F',
        backgroundColor:'#000',
        justifyContent:'center',
        alignItems:'center'
    },
    textHeader:{
        fontSize:30,
        color:'#fff',
        fontWeight:'bold',
    },
    containerImage:{
        flex:6,
        backgroundColor:'#fff',

    },
    image:{
        width:'100%',
        height:'100%',
    },
    containerTittle:{
        flex:1,
        backgroundColor:'#fff',
        paddingHorizontal:10,
        paddingVertical:10,
        borderBottomColor: 'black', 
        borderBottomWidth: 1, 
        justifyContent:'center',
    },
    textTitulo:{
        fontSize:26,
        color:'#000',
        fontWeight:'bold',
    },
    textTituloDescripcion:{
        fontSize:16,
        color:'#46494b',
        fontWeight:'normal',
    },
    containerSubtittle:{
        flex:4,
        backgroundColor:'#fff',
        paddingHorizontal:10,
        paddingVertical:10,
    },
    textSubtitulo:{
        fontSize:18,
        color:'#000',
        fontWeight:'normal',
        textAlign:'justify',
    },
    containerbutton:{
        flex:1,
        backgroundColor:'#fff',
        paddingHorizontal:10,
        paddingVertical:10,
    },
    button:{
        backgroundColor:'#04b6fb',
        alignItems:'center',
        justifyContent:'center',
        height:50,
        width:'100%',
    },
    buttonText:{
        fontSize:18,
        color:'#000',
        fontWeight:'bold',
    }
});
export default Card;